<?php

namespace Drupal\helpdesk_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\Service;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Refreshes issues from all external helpdesks.
 */
class RefreshIssues extends ControllerBase {

  /**
   * The helpdesk_integration.service service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * The refresh issues constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk_integration.service service.
   */
  public function __construct(Service $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('helpdesk_integration.service')
    );
  }

  /**
   * Builds the response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Destination when completed.
   */
  public function build(): RedirectResponse {
    /** @var \Drupal\user\UserInterface $user */
    $user = User::load($this->currentUser()->id());
    $this->service->sync('issues', NULL, $user, TRUE);
    return new RedirectResponse(Url::fromRoute('helpdesk_integration.helpdesk')->toString());
  }

}
