<?php

namespace Drupal\helpdesk_integration\Controller;

use Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Refreshes issues from all external helpdesks.
 */
class ResolveIssue extends ControllerBase {

  /**
   * The helpdesk_integration.service service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * The refresh issues constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk_integration.service service.
   */
  public function __construct(Service $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('helpdesk_integration.service')
    );
  }

  /**
   * TBD.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $helpdesk_issue
   *   The issue entity for which access should be checked.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function access(IssueInterface $helpdesk_issue): AccessResult {
    return AccessResult::allowedIf(!$helpdesk_issue->get('resolved')->value && $helpdesk_issue->hasUser(Drupal::currentUser()->id()));
  }

  /**
   * Builds the response.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $helpdesk_issue
   *   The issue entity which should be resolved.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Destination when completed.
   */
  public function build(IssueInterface $helpdesk_issue): RedirectResponse {
    $this->service->resolveIssue($helpdesk_issue);
    return new RedirectResponse(Url::fromRoute('entity.helpdesk_issue.canonical', [
      'helpdesk_issue' => $helpdesk_issue->id(),
    ])->toString());
  }

}
