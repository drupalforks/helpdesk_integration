<?php

namespace Drupal\helpdesk_integration\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\Service;
use Drupal\user\Entity\User;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create Helpdesk issue from a webform submission.
 *
 * @WebformHandler(
 *   id = "helpdesk_integration",
 *   label = @Translation("Helpdesk"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission to a helpdesk."),
 * )
 */
class Helpdesk extends WebformHandlerBase {

  /**
   * The helpdesk services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * Helpdesk constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\webform\WebformSubmissionConditionsValidatorInterface $conditions_validator
   *   The webform submission conditions (#states) validator.
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk services.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, Service $service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('helpdesk_integration.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'helpdesk' => '',
      'field_email' => '',
      'field_subject' => '',
      'field_description' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    $fields = [];
    foreach ($webform->getElementsInitializedAndFlattened() as $item) {
      $fields[$item['#webform_key']] = $item['#title'];
    }

    $form['helpdesk'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => $this->service->getHelpdesksForSelect(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['helpdesk'],
    ];
    $form['field_email'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store email address'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_email'],
    ];
    $form['field_subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store subject'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_subject'],
    ];
    $form['field_description'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to store description'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_description'],
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if (!$update && $webform_submission->getState() === WebformSubmissionInterface::STATE_COMPLETED) {
      $data = $webform_submission->getData();
      $email = $data[$this->configuration['field_email']];
      $user = user_load_by_mail($email);
      if (!$user) {
        $user = User::create([
          'name' => $email,
          'email' => $email,
        ]);
        $user->save();
      }

      $description = $data[$this->configuration['field_description']] . PHP_EOL . PHP_EOL . '<code>';
      foreach ($data as $key => $value) {
        if (!in_array($key, $this->configuration, TRUE)) {
          $description .= $key . ': ' . $value . PHP_EOL;
        }
      }
      $description .= '</code>';
      $issue = Issue::create([
        'helpdesk' => $this->configuration['helpdesk'],
        'uid' => $user->id(),
        'users' => [$user->id()],
        'title' => $data[$this->configuration['field_subject']],
        'body' => [
          'value' => $description,
          'format' => 'basic_html',
        ],
      ]);
      $issue->save();
    }
  }

}
