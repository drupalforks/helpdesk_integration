<?php

namespace Drupal\helpdesk_integration;

use Drupal\Component\Plugin\Exception\ExceptionInterface;
use Exception;

/**
 * Class HelpdeskPluginException.
 *
 * @package Drupal\helpdesk_integration
 */
class HelpdeskPluginException extends Exception implements ExceptionInterface {}
