<?php

namespace Drupal\helpdesk_integration;

use Drupal\comment\CommentInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\helpdesk_integration\Entity\Helpdesk;
use Drupal\user\UserInterface;
use Exception;

/**
 * Helpdesk Services.
 */
class Service {

  use StringTranslationTrait;

  /**
   * TBD.
   *
   * @var \Drupal\helpdesk_integration\PluginManager
   */
  protected $pluginManager;

  /**
   * TBD.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * TBD.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Service constructor.
   *
   * @param \Drupal\helpdesk_integration\PluginManager $plugin_manager
   *   The helpdesk plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   */
  public function __construct(PluginManager $plugin_manager, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    $this->pluginManager = $plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * Returns a list of active helpdesk entities.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   The list of active helpdesk instances.
   */
  public function getHelpdeskInstances(): array {
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface[] $instances */
    $instances = [];
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    foreach (Helpdesk::loadMultiple() as $helpdesk) {
      if ($helpdesk->status()) {
        $instances[] = $helpdesk;
      }
    }
    return $instances;
  }

  /**
   * Returns a list of active helpdesks for select lists in forms.
   *
   * @return array
   *   List of active helpdesk labels indexed by their helpdesk ID.
   */
  public function getHelpdesksForSelect(): array {
    $result = [];
    foreach ($this->getHelpdeskInstances() as $helpdeskInstance) {
      $result[$helpdeskInstance->id()] = $helpdeskInstance->label();
    }
    return $result;
  }

  /**
   * Returns a list of active helpdesk entities for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity for whom the list should be built.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface[]
   *   The list of active helpdesk instances.
   */
  public function getHelpdeskInstancesForUser(UserInterface $user): array {
    $instances = [];
    foreach ($this->getHelpdeskInstances() as $helpdesk) {
      if ($helpdesk->hasAccess($user)) {
        $instances[] = $helpdesk;
      }
    }
    return $instances;
  }

  /**
   * Get a list of all installed helpdesk plugins.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface[]
   *   The list of installed helpdesk plugins.
   */
  public function getPluginInstances(): array {
    $instances = [];
    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      try {
        $instances[$id] = $this->pluginManager->createInstance($id);
      }
      catch (PluginException $e) {
        // We can savely ignore this here.
      }
    }
    return $instances;
  }

  /**
   * Create and return a helpdesk plugin instance for the given ID.
   *
   * @param string $plugin_id
   *   The ID of the plugin which should be returned.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface
   *   The helpdesk plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPluginInstance($plugin_id): PluginInterface {
    /** @var \Drupal\helpdesk_integration\PluginInterface $instance */
    $instance = $this->pluginManager->createInstance($plugin_id);
    return $instance;
  }

  /**
   * Determine and return the correct helpdesk entity for the given issue.
   *
   * By running the rule engine, this determines the best matching helpdesk
   * entity. If there is only one helpdesk instance available for the issue
   * owner, that one will be selected. Otherwise the default helpdesk will
   * be used.
   *
   * This rule engine is intended to be extended with more sophisticated
   * conditions in later versions of this module.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity for which the helpdesk should be determined.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   The determined helpdesk entity.
   *
   * @throws \Exception
   */
  public function getHelpdesk(IssueInterface $issue): HelpdeskInterface {
    $helpdesks = $this->getHelpdeskInstancesForUser($issue->getOwner());
    if (empty($helpdesks)) {
      throw new Exception('Not allowed while no active helpdesk available.');
    }
    if (count($helpdesks) === 1) {
      return reset($helpdesks);
    }

    return $this->getDefaultHelpdesk();
  }

  /**
   * Find and return the default helpdesk entity.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   The default helpdesk entity.
   *
   * @throws \Exception
   */
  public function getDefaultHelpdesk(): HelpdeskInterface {
    foreach ($this->getHelpdeskInstances() as $helpdesk) {
      if ($helpdesk->isDefault()) {
        return $helpdesk;
      }
    }
    throw new Exception('Default helpdesk not available.');
  }

  /**
   * Mark an issue as resolved and submit change to remote helpdesk.
   *
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue entity to which should be marked as resolved.
   */
  public function resolveIssue(IssueInterface $issue): void {
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    $helpdesk = Helpdesk::load($issue->get('helpdesk')->value);
    try {
      $helpdesk->getPlugin()->resolveIssue($helpdesk, $issue);
      $issue
        ->set('resolved', TRUE)
        ->save();
    }
    catch (HelpdeskPluginException $e) {
      $this->messenger->addWarning($this->t('@name currently not available', [
        '@name' => $helpdesk->label(),
      ]));
    }
    catch (PluginException $e) {
      $this->messenger->addError($this->t('Plugin for @name no longer available', [
        '@name' => $helpdesk->label(),
      ]));
    }
    catch (EntityStorageException $e) {
      $this->messenger->addError($this->t('Unable to save resolved status locally'));
    }

  }

  /**
   * Add the new comment to its issue on the remote helpdesk.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   The comment entity.
   */
  public function addCommentToIssue(CommentInterface $comment): void {
    if ($comment->bundle() !== 'helpdesk_issue_comment' || !empty($comment->get('field_extid')->value)) {
      return;
    }
    /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
    $issue = $comment->getCommentedEntity();
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    $helpdesk = Helpdesk::load($issue->get('helpdesk')->value);
    try {
      $helpdesk->getPlugin()->addCommentToIssue($helpdesk, $issue, $comment);
      $comment->save();
    }
    catch (HelpdeskPluginException $e) {
      $this->messenger->addWarning($this->t('@name currently not available', [
        '@name' => $helpdesk->label(),
      ]));
    }
    catch (PluginException $e) {
      $this->messenger->addError($this->t('Plugin for @name no longer available', [
        '@name' => $helpdesk->label(),
      ]));
    }
    catch (EntityStorageException $e) {
      $this->messenger->addError($this->t('Unable to save external comment id'));
    }
  }

  /**
   * Helper function to update/sync objects with remote helpdesk.
   *
   * @param string $type
   *   The object type to update, can be "users" or "issues".
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity to/from which the update/sync should be performed.
   * @param \Drupal\user\UserInterface|null $user
   *   Optional, the user entity which is related to the update/sync.
   * @param bool $force
   *   Optional, if TRUE the update/sync is performed regardless of any
   *   configured timeouts between similar tasks.
   * @param array|null $batch
   *   If an array is given, this callback does NOT update users directly but
   *   adds a Batch API compatible operation to the array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  protected function updateAll($type, HelpdeskInterface $helpdesk, UserInterface $user = NULL, $force = FALSE, array &$batch = NULL) {
    $plugin = $helpdesk->getPlugin();
    switch ($type) {
      case 'users':
        $plugin->updateAllUsers($helpdesk, $user, $batch);
        break;

      case 'issues':
        $plugin->updateAllIssues($helpdesk, $user, $force);
        break;
    }
  }

  /**
   * Helper function to update/sync objects with one or all remote helpdesk(s).
   *
   * @param string $type
   *   The object type to update, can be "users" or "issues".
   * @param \Drupal\helpdesk_integration\HelpdeskInterface|null $helpdesk
   *   Optional, the helpdesk entity to/from which the update/sync should be
   *   performed. If not provided, the update/sync will be performed with all
   *   active helpdesks.
   * @param \Drupal\user\UserInterface|null $user
   *   Optional, the user entity which is related to the update/sync.
   * @param bool $force
   *   Optional, if TRUE the update/sync is performed regardless of any
   *   configured timeouts between similar tasks.
   * @param array|null $batch
   *   If an array is given, this callback does NOT update users directly but
   *   adds a Batch API compatible operation to the array.
   */
  public function sync($type, HelpdeskInterface $helpdesk = NULL, UserInterface $user = NULL, $force = FALSE, array &$batch = NULL) {
    $helpdesks = ($helpdesk === NULL) ?
      $this->getHelpdeskInstances() :
      [$helpdesk];

    foreach ($helpdesks as $helpdesk) {
      if ($user !== NULL && !$helpdesk->hasAccess($user)) {
        continue;
      }
      try {
        $this->updateAll($type, $helpdesk, $user, $force, $batch);
        $this->messenger->addStatus($this->t('Synced @type with @name', [
          '@type' => $type,
          '@name' => $helpdesk->label(),
        ]));
      }
      catch (HelpdeskPluginException $e) {
        $this->messenger->addWarning($this->t('@name currently not available', [
          '@name' => $helpdesk->label(),
        ]));
      }
      catch (PluginException $e) {
        $this->messenger->addError($this->t('Plugin for @name no longer available', [
          '@name' => $helpdesk->label(),
        ]));
      }
    }
  }

}
