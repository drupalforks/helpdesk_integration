<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Exception;

/**
 * Helpdesk form.
 *
 * @property \Drupal\helpdesk_integration\HelpdeskInterface $entity
 */
class Helpdesk extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\helpdesk_integration\Service $service */
    $service = Drupal::service('helpdesk_integration.service');
    $plugins = $service->getPluginInstances();
    $isFirst = empty($service->getHelpdeskInstances());
    $options = [];
    foreach ($plugins as $plugin) {
      $options[$plugin->getPluginId()] = $plugin->label();
    }
    if ($this->entity->isNew()) {
      $default_plugin = count($options) === 1 ? key($options) : '';
      $this->entity->set('status', TRUE);
    }
    else {
      $default_plugin = $this->entity->get('plugin_id');
    }
    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Backend'),
      '#options' => $options,
      '#default_value' => $default_plugin,
      '#disabled' => !$this->entity->isNew() || count($options) === 1,
      '#required' => TRUE,
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the helpdesk.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\helpdesk_integration\Entity\Helpdesk::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
      '#disabled' => $this->entity->isDefault(),
      '#states' => [
        'disabled' => [
          ':input[name="default"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default'),
      '#default_value' => $this->entity->isDefault() || $isFirst,
      '#disabled' => $this->entity->isDefault() || $isFirst,
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the helpdesk.'),
    ];

    $form['#tree'] = TRUE;

    foreach ($plugins as $plugin) {
      $plugin_id = $plugin->getPluginId();
      $required = [
        '#states' => [
          'required' => [
            'select[name="plugin_id"]' => ['value' => $plugin_id],
          ],
        ],
      ];
      $form['details_' . $plugin_id] = [
          '#type' => 'container',
          '#states' => [
            'visible' => [
              'select[name="plugin_id"]' => ['value' => $plugin_id],
            ],
          ],
        ] + $plugin->buildConfigurationForm($this->entity, $required);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    foreach ($this->entity->{'details_' . $this->entity->get('plugin_id')}
    as $key => $value) {
      $this->entity->set($key, $value);
    }

    /** @var Drupal\helpdesk_integration\Service $service */
    $service = Drupal::service('helpdesk_integration.service');
    foreach ($service->getPluginInstances() as $plugin) {
      unset($this->entity->{'details_' . $plugin->getPluginId()});
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\helpdesk_integration\Service $service */
    $service = Drupal::service('helpdesk_integration.service');
    try {
      $defaultHelpdesk = $service->getDefaultHelpdesk();
    } catch (Exception $e) {
      $defaultHelpdesk = NULL;
    }
    if ($this->entity->isDefault()) {
      $this->entity->set('status', TRUE);
    }
    $result = parent::save($form, $form_state);
    if ($defaultHelpdesk && $this->entity->isDefault() && $defaultHelpdesk->id() !== $this->entity->id()) {
      // We get a new default.
      $defaultHelpdesk
        ->set('default', FALSE)
        ->save();
      $this->messenger()->addStatus($this->t('Default helpdesk changed.'));
    }

    $message_args = ['%label' => $this->entity->label()];
    $message = $result === SAVED_NEW
      ? $this->t('Created new helpdesk %label.', $message_args)
      : $this->t('Updated helpdesk %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirect('entity.helpdesk.collection');

    if ($result === SAVED_NEW) {
      $batch = [
        'title' => $this->t('Synchronizing all users...'),
        'operations' => [],
      ];
      $service->sync('users', $this->entity, NULL, FALSE, $batch);
      batch_set($batch);
    }
    return $result;
  }

}
