<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\helpdesk_integration\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Refreshes users to all external helpdesks.
 */
class RefreshUsers extends ConfirmFormBase {

  /**
   * The helpdesk_integration.service service.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * The refresh users constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk_integration.service service.
   */
  public function __construct(Service $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('helpdesk_integration.service')
    );
  }

  public function getQuestion() {
    return $this->t('Do you want to synchronize all users with your helpdesk(s)?');
  }

  public function getCancelUrl() {
    return Url::fromRoute('entity.helpdesk.collection');
  }

  public function getFormId() {
    return 'helpdesk_refresh_users_confirm';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Synchronizing all users...'),
      'operations' => [],
    ];
    $this->service->sync('users', NULL, NULL, FALSE, $batch);
    batch_set($batch);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
